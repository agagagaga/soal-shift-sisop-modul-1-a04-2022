# soal-shift-sisop-modul-1-a04-2022 

|     NRP    |     Nama    |
| :--------- |:------------    |
| 5025201098 | Ibra Abdi Ibadihi
| 5025201184 | Cahyadi Surya Nugraha |
| 5025201007 | Sejati Bakti Raga

# soal shift sisop modul 1 A04 2022

# Soal 1
Output yang diinginkan dari soal 1 adalah untuk membuat sistem register dan login. Dimana setiap kegiatan login atau register
akan disimpan pada sebuah log.txt sesuai dengan status dari kegiatan entah itu login. Detail yang disimpan dalam log sendiri adalah
tanggal dan waktu kegiatan serta username yang melakukan kegiatan tersebut.
Ketika user berhasil login, terdapat 2 command yang nantinya akan dijalankan, yaitu att dan dl. 
Pilihan att akan menampilkan berapa banyak percobaan yang telah dilakukan user untuk login (baik gagal maupun sukses).
Sedangkan, Pilihan dl akan mendownload image dari web yang telah disediakan (https://loremflickr.com/320/240 )sebanyak N kali, 
kemudian image tersebut akan disimpan di dalam zip.

## Register 
Setiap register yang sukses akan disimpan pada /users/user.txt . 
User akan menginputkan username dan password. Terdapat beberapa persyaratan untuk pembuatan password, yaitu:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumerical
- Tidak boleh sama dengan username

Pada sistem register, yang paling pertama dilakukan adalah memastikan bahwa folder atau file dari output yang akan di redirect nanti ada, maka dari itu dibuatlah fungsi checkFileExist yang berfungsi untuk mengecek apakah folder atau folder output nanti sudah ada atau belum jika belum maka buatkan folder dan file tersebut menggunakan mkdir -p (-p ini untuk menghindari error dengan membuat parent direktorinya jika belum ada) dan menggunakan touch untuk membuat filenya.

Hal kedua yang harus dilakukan adalah mengecek apakah user yang diinput sudah terdaftar di user.txt dengan menggunakan grep -q -w (-w untuk mencocokan seluruh kata). Jika ditemukan maka program akan mengoutputkan "User already exist" dan melakukan pencatatan pada log.txt bahwa kegiatan tidak berhasil ($dates $time REGISTER:ERROR User already exist) dan program akan terhenti.

Jika tidak ditemukan maka sistem register akan melakukan validasi password dengan user akan diminta untuk memasukkan passwordnya sebanyak dua kali yang akan digunakan untuk memvalidasi dan memastikan kembali terhadap user bahwa user tersebut akan menggunakan password yang dituliskan pada field password sebelumnya. Pengecekan kesamaan password dan confirmpassword menggunakan operasi aritmatika !=, dimana jika hasilnya true atau artinya tidak sama maka akan mengoutputkan ke terminal "Password is not matches" dan melakukan exit. Jika bernilai false maka program akan lanjut ke tahap selanjutnya.

Tahap selanjutnya adalah mengecek syarat dari password yang telah diberikan yaitu:
- Mengecek minimal 8 karakter dengan mengecek apakah panjang karakter lebih sedikit dari (-lt) 8 atau tidak. Jika iya, maka proses dihentikan dan mengeluarkan message "Password length should at least be 8 characters", jika tidak lebih sedikit, maka lanjutkan untuk syarat selanjutnya.
- Mengecek minimal 1 huruf kapital pada password dengan menggunakan grep pada password. Jika tidak ditemukan maka akan mengeluarkan message "Password must contain at least one uppercase", jika memenuhi maka melanjutkan ke syarat berikutnya
- Mengecek minimal 1 huruf kecil pada password dengan menggunakan grep pada password. Jika tidak ditemukan maka akan mengeluarkan message "Password must contain at least one lowercase", jika memenuhi maka melanjutkan ke SSproses pencatatan log

Ketika semua persyaratan password dan username terpenuhi, maka print pesan log di log.txt dengan format MM/DD/YY HH:MM:SS  REGISTER:
INFO User **USERNAME** registered successfully. Dan disimpan pada user.txt dengan format MM/DD/YY HH:MM:SS **Username** **Password**

Proses Register selesai.

## Login

