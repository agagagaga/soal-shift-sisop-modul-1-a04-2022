#!/bin/bash

checkPass(){
    if [[ $pass != $confirmpass ]]; then
        echo "Password is not matches"
        exit 0
    elif [[ $username == $pass ]]; then
        echo "Password cannot be the same with username"
        exit 0
    elif [[ $len -lt 8 ]]; then
        echo "Password length should at least be 8 characters"
        exit 0
    fi

    echo $pass | grep -q [A-Z] 

    if test $? -ne 0 ; then
        echo "Password must contain at least one uppercase"
        exit 0
    fi

    echo $pass | grep -q [a-z] 

    if test $? -ne 0 ; then
        echo "Password must contain at least one lowercase"
        exit 0
    fi

    echo $pass | grep -q [0-9] 

    if test $? -ne 0 ; then
        echo "Password must contain at least one number"
        exit 0
    fi

    echo "Success created account"
	echo $dates $time $username $pass >> $txtUser
	echo $dates $time REGISTER:INFO User $username registered succesfully >> $txtLog
}

checkFileExist(){
    local PATHUSER="./users"
    local PATHLOG="./soal-shift-sisop-modul-1-a04-2022"
    if [ ! -f $PATHUSER ]; then
        mkdir -p -v $PATHUSER
        touch ./users/user.txt
    fi
    
    if [ ! -f $PATHLOG ]; then
        mkdir -p -v $PATHLOG
        touch ./soal-shift-sisop-modul-1-a04-2022/log.txt
        echo ""
    fi
}

checkUserExist(){
    if grep -q -w $username "$txtUser" ; then
        echo "User already exist"
        echo $dates $time REGISTER:ERROR "User already exist" >> $txtLog
        exit 0
    fi
}

read -p "Enter Username: " username
read -s -p "Enter Password: " pass
echo ""
read -s -p "Confirm Password: " confirmpass
echo ""

len="${#pass}"
txtUser=./users/user.txt
txtLog=./soal-shift-sisop-modul-1-a04-2022/log.txt

checkFileExist 

dates=$(date +%D)
time=$(date +%T)

checkUserExist
checkPass
